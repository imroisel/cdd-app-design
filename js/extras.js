$(function () {
    $('#fecha-evento').datetimepicker({
        inline: true,
        sideBySide: true,
        format: 'dd MM yyyy'
    });
})(jQuery);
